#include<stdio.h>
struct student{
    char fName[20];
    char subject[50];
    int marks;
};
int studentCount();
void getStudentDetails(struct student *studentArray,int);
void printStudentDetails(struct student *studentArray,int);

int main(){
    int n=studentCount();
    struct student studentArray[n];
    getStudentDetails(studentArray,n);
    printStudentDetails(studentArray,n);
return 0;
}

int studentCount(){
    int n=0;
    printf("Input the number of Students : ");
    scanf("%d",&n);
    fflush(stdin);
    if(n<5){
        printf("No of students should not be less than 5 !\n");
        return studentCount();
    }
    return n;
}
void getStudentDetails(struct student *studentArray,int n){
        
    for(int i=0;i<n;i++){
        printf("Student %d \n",i+1);
        printf("First Name   : ");
        scanf("%s",&studentArray[i].fName);
        fflush(stdin);
        printf("Subject      : ");
        scanf("%[^\n]%*c",&studentArray[i].subject);
        fflush(stdin);
        printf("Marks        : ");
        scanf("%d",&studentArray[i].marks);
        printf("\n");

    }
    
}
void printStudentDetails(struct student *studentArray,int n){
    
    for(int i=0;i<n;i++){
        printf("\n==============================================\n");
        printf("Student %d \n",i+1);
        printf("First Name  : %s\n",studentArray[i].fName);
        printf("Subject     : %s\n",studentArray[i].subject);
        printf("Marks       : %d\n",studentArray[i].marks);

    }    
}