#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int Cols1,Rows1,Cols2,Rows2;

/*int* getMatrix(){
    int array[Rows][Cols];

    return array;
}*/
int addMatrices(int a[Rows1][Cols1],int b[Rows2][Cols2]){
    if(Rows1!=Rows2 && Cols1!=Cols2 ){
        printf("These matrices cannot be added..!\n\n");
        return 0;
    }
    else{
        int added[Rows1][Cols1];
        for(int i=0;i<Rows1;i++){
            for(int j=0;j<Cols1;j++){
                added[i][j]=a[i][j]+b[i][j];
            }
        }
        printf("============== Addition of Two Matrices ==============\n");
        for(int i=0;i<Rows1;i++){
            for(int j=0;j<Cols1;j++){
                printf("%d\t",added[i][j]);
            }
            printf("\n");
        }printf("\n");
    }
    
}
int multiply(int a[Rows1][Cols1],int b[Rows2][Cols2]){
    int multiplied[Rows1][Cols2];

    

    if(Cols1!=Rows2){
        printf("These matrices cannot be multiplied..!\n\n");
        return 1;
    }
    else{
        
        for(int i=0;i<Rows1;i++){
            for(int j=0;j<Cols2;j++){
                
                for(int k=0;k<Rows2;k++){
                    multiplied[i][j]+=a[i][k]*b[k][j];
                }
                
            
            }    
        }        
    }
    printf("=========== Multiplication of Two Matrices ===========\n");
    for(int i=0;i<Rows1;i++){
        for(int j=0;j<Cols2;j++){
            printf("%d\t",multiplied[i][j]);
        }
        printf("\n");
    }

}
int main(){
    char *ch=" "; 
    printf("Input the number of Rows of Columns of first matrix :");
    scanf("%d %d",&Rows1,&Cols1);
    if(Rows1<=0 || Cols1<=0){
        printf("invalid Entry");
        return 0;
    } 
    
    int arrayOne[Rows1][Cols1];
   
   printf("\nEnter elements of 1st matrix:\n");
    for (int i = 0; i < Rows1; i++){
        for (int j = 0; j < Cols1; j++) {
            printf("Enter element a%d%d: ", i + 1, j + 1);
            scanf("%d", &arrayOne[i][j]);
        }
    }
    printf("Input the number of Rows and Columns of Second matrix :");
    scanf("%d %d",&Rows2,&Cols2);

    if(Cols2<=0 || Rows2<=0){
        printf("invalid Entry");
        return 0;
    }
    int arrayTwo[Rows2][Cols2]; 
    printf("Enter elements of 2nd matrix:\n");
    for (int i = 0; i < Rows2; i++){
        for (int j = 0; j < Cols2; j++) {
            printf("Enter element a%d%d: ", i + 1, j + 1);
            scanf("%d", &arrayTwo[i][j]);
        }
    }
    printf("\n");
    addMatrices(arrayOne,arrayTwo);
    multiply(arrayOne,arrayTwo);
}
